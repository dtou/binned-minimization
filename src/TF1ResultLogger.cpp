#ifndef TF1RESULTLOGGER_CPP
#define TF1RESULTLOGGER_CPP

#include "TF1ResultLogger.h"
#include "TFitResult.h"
#include <stdexcept>
#include <iostream>

const TString TF1ResultLogger::parListBranchName = "name";
const TString TF1ResultLogger::parListTreeName   = "ParameterNames";

void TF1ResultLogger::LogFit(const TFitResultPtr & fitResult) {
    CheckResultConsistency(fitResult);
    LogVariables(fitResult);
    LogFitStatus(fitResult);
    LogMatrices(fitResult);
}

void TF1ResultLogger::CheckResultConsistency(const TFitResultPtr & fitResult) {
    if (m_listOfParNames.size() == 0) {
        // First fit to log
        RecordParNames(fitResult);
        RecordNPars(fitResult);
    } else {
        // The rest
        CheckParCount(fitResult);
        CheckParNames(fitResult);
    }
}

void TF1ResultLogger::RecordParNames(const TFitResultPtr & fitResult) {
    TString parName;
    for (int i = 0; i < fitResult->NPar(); i++) {
        parName = TString(fitResult->GetParameterName(i));
        m_listOfParNames.push_back(parName);
    }
}

void TF1ResultLogger::RecordNPars(const TFitResultPtr & fitResult) {
    m_numberOfVariables = fitResult->NPar();
}

void TF1ResultLogger::CheckParCount(const TFitResultPtr & fitResult) const {
    int thisFitResultParCount     = fitResult->NPar();
    int previousFitResultParCount = m_listOfParNames.size();
    if (thisFitResultParCount != previousFitResultParCount) { 
        std::string error_message("TF1ResultLogger\t This TFitResultPtr contains different parameters compared to the last TFitResultPtr");
        throw std::logic_error(error_message);
    }
}

void TF1ResultLogger::CheckParNames(const TFitResultPtr & fitResult) const {
    TString thisFitParName;
    TString lastFitParName;
    for (int i = 0; i < m_listOfParNames.size(); i++) {
        thisFitParName = TString(fitResult->GetParameterName(i));
        lastFitParName = TString(m_listOfParNames[i]);
        if (thisFitParName != lastFitParName) { 
            std::string error_message("TF1ResultLogger\t This TFitResultPtr contains parameters with names that do not match the previous TFitResultPtr.");
            throw std::logic_error(error_message);
        }
    }
}

void TF1ResultLogger::LogVariables(const TFitResultPtr & fitResult) {
    for (int i = 0; i < fitResult->NPar(); i++) {
        m_values.push_back(fitResult->Value(i));
        m_errors.push_back(fitResult->Error(i));
    }
}

void TF1ResultLogger::LogFitStatus(const TFitResultPtr & fitResult) {
    int covarianceQuality = fitResult->CovMatrixStatus();
    int fitStatus         = fitResult->IsValid() ? 0 : -1;
    double edm            = fitResult->Edm();
    double minNll         = fitResult->MinFcnValue();

    m_covarianceQualities.push_back(covarianceQuality);
    m_fitStatuses.push_back(fitStatus);
    m_edm.push_back(edm);
    m_minNll.push_back(minNll);
}

void TF1ResultLogger::LogMatrices(const TFitResultPtr & fitResult) {
    TMatrixDSym correlationMatrix = fitResult->GetCorrelationMatrix();
    TMatrixDSym covarianceMatrix  = fitResult->GetCovarianceMatrix();
    m_correlationMatrices.push_back(correlationMatrix);
    m_covarianceMatrices.push_back(covarianceMatrix);
}

void TF1ResultLogger::SaveResults(TString fileName, TString treeName) {
    CheckLogStarted();
    InstantiateOutputs(fileName, treeName);
    AllocateContainers();
    AttachBranchesToTree();
    FillTree();
    FillParNameTree();
    WriteAndClose();
    PrintSuccessfulOutput(fileName, treeName);
};

void TF1ResultLogger::CheckLogStarted() const {
    if (m_covarianceQualities.empty()) { 
        std::string error_message("TF1ResultLogger\t Tried to save output but nothing was logged.");
        throw std::logic_error(error_message);
    }
}

void TF1ResultLogger::InstantiateOutputs(const TString & fileName, const TString & treeName) {
    m_outFile     = TFile::Open(fileName.Data(), "RECREATE");
    m_outTree     = new TTree(treeName, "");
    m_parNameTree = new TTree(parListTreeName, "");
}

void TF1ResultLogger::AllocateContainers() {
    m_singleFitResult.values = (double *) malloc((m_numberOfVariables) * sizeof(double));
    m_singleFitResult.errors = (double *) malloc((m_numberOfVariables) * sizeof(double));
    m_singleFitResult.correlationMatrix.ResizeTo(m_correlationMatrices[0]);
    m_singleFitResult.covarianceMatrix.ResizeTo(m_covarianceMatrices[0]);
}

void TF1ResultLogger::AttachBranchesToTree() {
    for (int i = 0; i < m_numberOfVariables; i++) { AddBranch(m_listOfParNames[i], i); }
    m_outTree->Branch("covarianceQuality", &m_singleFitResult.covarianceQuality);
    m_outTree->Branch("fitStatus", &m_singleFitResult.fitStatus);
    m_outTree->Branch("edm", &m_singleFitResult.edm);
    m_outTree->Branch("minNll", &m_singleFitResult.minNll);
    m_outTree->Branch("correlationMatrix", &m_singleFitResult.correlationMatrix);
    m_outTree->Branch("covarianceMatrix", &m_singleFitResult.covarianceMatrix);
}

void TF1ResultLogger::AddBranch(const TString & name, int index) {
    TString valueName = RemoveMathFormula(name);
    TString errorName = valueName + "_error";
    m_outTree->Branch(valueName.Data(), m_singleFitResult.values + index);
    m_outTree->Branch(errorName.Data(), m_singleFitResult.errors + index);
}

TString TF1ResultLogger::RemoveMathFormula(TString initialName) const {
    TString newString = initialName.ReplaceAll("-", "_");
    return newString;
}

void TF1ResultLogger::FillTree() {
    int numberOfFits = CalculateNumberOfFits();
    for (int index = 0; index < numberOfFits; index++) { FillEvent(index); }
}

int TF1ResultLogger::CalculateNumberOfFits() const {
    int numberOfValues = m_values.size();
    return numberOfValues / (m_numberOfVariables);
}

void TF1ResultLogger::FillEvent(int index) {
    int offset = index * m_numberOfVariables;
    for (int i = 0; i < m_numberOfVariables; i++) {
        m_singleFitResult.values[i] = m_values[offset + i];
        m_singleFitResult.errors[i] = m_errors[offset + i];
    }
    m_singleFitResult.covarianceQuality = m_covarianceQualities[index];
    m_singleFitResult.fitStatus         = m_fitStatuses[index];
    m_singleFitResult.edm               = m_edm[index];
    m_singleFitResult.minNll            = m_minNll[index];
    m_singleFitResult.correlationMatrix = m_correlationMatrices[index];
    m_singleFitResult.covarianceMatrix  = m_covarianceMatrices[index];
    m_outTree->Fill();
}

void TF1ResultLogger::FillParNameTree() {
    TString parName;
    m_parNameTree->Branch(parListBranchName, &parName);
    for (int i = 0; i < m_listOfParNames.size(); i++) {
        parName = RemoveMathFormula(m_listOfParNames[i]);
        m_parNameTree->Fill();
    } 
}

void TF1ResultLogger::WriteAndClose() {
    m_outTree->Write();
    m_parNameTree->Write();
    m_outFile->Close();
    free(m_singleFitResult.values);
    free(m_singleFitResult.errors);
}

void TF1ResultLogger::PrintSuccessfulOutput(const TString & fileName, const TString & treeName) const {
    std::cout << "TF1ResultLogger" << std::endl;
    std::cout << "File\t" << fileName.Data() << std::endl;
    std::cout << "Tuple\t" << treeName.Data() << std::endl;
}

#endif
