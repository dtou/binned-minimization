#ifndef BININTEGRATOR_CPP
#define BININTEGRATOR_CPP

#include "BinIntegrator.h"
#include "RooRealVar.h"
#include "RooAbsPdf.h"
#include "RooArgSet.h"

double BinIntegrator::evaluatePDF(double x, void * params){
    ((RooRealVar**)params)[0]->setVal(x);
    double normval = ((RooAbsPdf**)params)[1]->getVal(((RooArgSet**)params)[2]);
    return normval;
}

#endif
