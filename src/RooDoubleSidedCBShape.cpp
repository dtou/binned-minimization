#include "RooFit.h"

#include "Riostream.h"
#include <math.h>

#include "RooAbsReal.h"
#include "RooDoubleSidedCBShape.h"
#include "RooMath.h"
#include "RooRealVar.h"
#include "TMath.h"

#include "TError.h"

using namespace std;

ClassImp(RooDoubleSidedCBShape);

RooDoubleSidedCBShape::RooDoubleSidedCBShape(const char * name, const char * title, RooAbsReal & _x, RooAbsReal & _mu, RooAbsReal & _sigma, RooAbsReal & _alphaLow, RooAbsReal & _nLow, RooAbsReal & _alphaHigh, RooAbsReal & _nHigh)
    : RooAbsPdf(name, title)
    , x("x", "Dependent", this, _x)
    , mu("mu", "Mu", this, _mu)
    , sigma("sigma", "Sigma", this, _sigma)
    , alphaLow("alphaLow", "AlphaLow", this, _alphaLow)
    , nLow("nLow", "Low Tail Order", this, _nLow)
    , alphaHigh("alphaHigh", "AlphaLow", this, _alphaHigh)
    , nHigh("nHigh", "High Tail Order", this, _nHigh) {}

RooDoubleSidedCBShape::RooDoubleSidedCBShape(const RooDoubleSidedCBShape & other, const char * name)
    : RooAbsPdf(other, name)
    , x("x", this, other.x)
    , mu("mu", this, other.mu)
    , sigma("sigma", this, other.sigma)
    , alphaLow("alphaLow", this, other.alphaLow)
    , nLow("nLow", this, other.nLow)
    , alphaHigh("alphaHigh", this, other.alphaHigh)
    , nHigh("nHigh", this, other.nHigh) {}

Double_t RooDoubleSidedCBShape::evaluate() const {
    double t      = (x - mu) / sigma;
    double result = 0;
    if (t < -alphaLow) {
        double a = pow(nLow / alphaLow, nLow) * exp(-0.5 * alphaLow * alphaLow);
        double b = nLow / alphaLow - alphaLow;
        result   = a / pow(b - t, nLow);
    } else if (t >= -alphaLow && t < alphaHigh) {
        result = exp(-0.5 * t * t);
    } else if (t >= alphaHigh) {
        double a = pow(nHigh / alphaHigh, nHigh) * exp(-0.5 * alphaHigh * alphaHigh);
        double b = nHigh / alphaHigh - alphaHigh;
        result   = a / pow(b + t, nHigh);
    }
    return result;
}

Int_t RooDoubleSidedCBShape::getAnalyticalIntegral(RooArgSet & allVars, RooArgSet & analVars, const char * rangeName) const {
    if (matchArgs(allVars, analVars, x)) return 1;

    return 0;
}

double RooDoubleSidedCBShape::ComputeCDF(double t) const {
    double result;
    double aLow = pow(nLow / alphaLow, nLow) * exp(-0.5 * alphaLow * alphaLow);
    double bLow = nLow / alphaLow - alphaLow;
    // Low tail region
    if (t <= -alphaLow) {
        bool useLog = (abs(nLow - 1.0) < 1.0e-5);
        if (useLog) {
            result = -1. * aLow * sigma * log(bLow - t);
        } else {
            result = (aLow * sigma / (nLow - 1)) / pow(bLow - t, nLow - 1);
        }
    } else if (t > -alphaLow && t <= alphaHigh) {
        bool lowUseLog = (abs(nLow - 1.0) < 1.0e-5);
        // Integral of lower tail
        if (lowUseLog) {
            result = -1. * aLow * sigma * log(nLow / alphaLow);
        } else {
            result = (aLow * sigma / (nLow - 1)) / pow(nLow / alphaLow, nLow - 1);
        }
        result += sigma * sqrtPi * invSqrt2 * (erf(t * invSqrt2) + erf(alphaLow * invSqrt2));
    } else {
        bool   lowUseLog  = (abs(nLow - 1.0) < 1.0e-5);
        bool   highUseLog = (abs(nHigh - 1.0) < 1.0e-5);
        double aHigh      = pow(nHigh / alphaHigh, nHigh) * exp(-0.5 * alphaHigh * alphaHigh);
        double bHigh      = nHigh / alphaHigh - alphaHigh;
        // Integral of lower tail
        if (lowUseLog) {
            result = -1. * aLow * sigma * log(nLow / alphaLow);
        } else {
            result = (aLow * sigma / (nLow - 1)) / pow(nLow / alphaLow, nLow - 1);
        }
        // Integral of Gaussian core
        result += sigma * sqrtPi * invSqrt2 * (erf(alphaHigh * invSqrt2) + erf(alphaLow * invSqrt2));
        if (highUseLog) {
            result += aHigh * sigma * (log(bHigh + t) - log(nHigh / alphaHigh));
        } else {
            result += (aHigh * sigma / (1. - nHigh)) * (1. / pow(bHigh + t, nHigh - 1.) - 1. / pow(nHigh / alphaHigh, nHigh - 1.));
        }
    }
    return result;
}

Double_t RooDoubleSidedCBShape::analyticalIntegral(Int_t code, const char * rangeName) const {
    double tMin = (x.min(rangeName) - mu) / sigma;
    double tMax = (x.max(rangeName) - mu) / sigma;

    double zMin = ComputeCDF(tMin);
    double zMax = ComputeCDF(tMax);

    return (zMax - zMin);
}

Int_t RooDoubleSidedCBShape::getMaxVal(const RooArgSet & vars) const {
    RooArgSet dummy;

    if (matchArgs(vars, dummy, x)) { return 1; }
    return 0;
}

Double_t RooDoubleSidedCBShape::maxVal(Int_t code) const {
    R__ASSERT(code == 1);

    // The maximum value for given (m0,alpha,n,sigma)
    return 1.0;
}
