#ifndef MODEL_CPP
#define MODEL_CPP

#include "Model.h"

Model::Model(){
    m_Nsignal = new RooRealVar("nSignal", "nSignal", 0);
    m_Nbackground = new RooRealVar("nBackground", "nBackground", 0);
    m_aLow = new RooRealVar("aLow", "aLow", 0);
    m_nLow = new RooRealVar("nLow", "nLow", 0);
    m_mean = new RooRealVar("mean", "mean", 0);
    m_sigma = new RooRealVar("sigma", "sigma", 0);
    m_aHigh = new RooRealVar("aHigh", "aHigh", 0);
    m_nHigh = new RooRealVar("nHigh", "nHigh", 0);
    m_slope = new RooRealVar("slope", "slope", 0);

    m_mean->setConstant(false);
    m_sigma->setConstant(false);
    m_slope->setConstant(false);
    m_Nsignal->setConstant(false);
    m_Nbackground->setConstant(false);

    m_signal = nullptr;
    m_background = nullptr;
    m_pdf = nullptr;
}

Model::Model(RooRealVar& observable) : Model() {
    SetObservable(observable);
}

void Model::SetObservable(RooRealVar& observable) {
    DeletePDFIfNotNull();
    m_signal = new RooDoubleSidedCBShape("model_signal", "model_signal",
                                         observable, *m_mean, *m_sigma, 
                                         *m_aLow, *m_nLow, *m_aHigh, *m_nHigh);
    m_background = new RooExponential("model_background", "model_background",
                                      observable, *m_slope);
    m_pdf = new RooAddPdf("model", "model", RooArgList(*m_signal, *m_background)
                                          , RooArgList(*m_Nsignal, *m_Nbackground));
}

void Model::DeletePDFIfNotNull(){
    if (m_signal != nullptr){
        delete m_signal;
        m_signal = nullptr;
    }
    if (m_background != nullptr){
        delete m_background;
        m_background = nullptr;
    }
    if (m_pdf != nullptr){
        delete m_pdf;
        m_pdf = nullptr;
    }
}

void Model::ConfigureSignal(int n_signal, double mean, double sigma,
                            double aLow, double nLow, double aHigh, double nHigh){
    m_Nsignal->setVal(n_signal);
    m_mean->setVal(mean);
    m_sigma->setVal(sigma);
    m_aLow->setVal(aLow);
    m_nLow->setVal(nLow);
    m_aHigh->setVal(aHigh);
    m_nHigh->setVal(nHigh);
    m_Nsignal->setError(0.05*n_signal);
    m_mean->setError(0.05*mean);
    m_sigma->setError(0.05*sigma);
    m_aLow->setError(0.05*aLow);
    m_nLow->setError(0.05*nLow);
    m_aHigh->setError(0.05*aHigh);
    m_nHigh->setError(0.05*nHigh);
}

void Model::ConfigureBackground(int n_background, double slope){
    m_Nbackground->setVal(n_background);
    m_slope->setVal(slope);
    m_Nbackground->setError(0.05*n_background);
    m_slope->setError(0.05*slope);
}

RooAbsPdf * Model::GetPDF(){
    return (RooAbsPdf*)m_pdf;
}

RooDoubleSidedCBShape * Model::GetSignal(){
    return m_signal;
}

RooExponential * Model::GetBackground(){
    return m_background;
}

Model::~Model(){
    DeletePDFIfNotNull();
    delete m_Nsignal;
    delete m_Nbackground;
    delete m_aLow;
    delete m_nLow;
    delete m_mean;
    delete m_sigma;
    delete m_aHigh;
    delete m_nHigh;
    delete m_slope;
}

#endif
