#ifndef ADAPTIVEGGKNLL_CPP
#define ADAPTIVEGGKNLL_CPP

#include "AdaptivegGKNLL.h"
#include "RooAbsBinning.h"
#include "RooAbsDataStore.h"
#include "BinIntegrator.h"
#include "RooCmdConfig.h"
#include "RooAddition.h"
#include "RooConstraintSum.h"

#include <numeric>
#include <iostream>

RooAbsReal * BinnedAdaptivegGKNLL::CreateNLL(RooAbsPdf& _model, RooDataHist& _binnedData, RooRealVar& observable, const RooLinkedList& cmdList) {
    // Select the pdf-specific commands 
    RooCmdConfig pc(Form("BinnedAdaptivegGKNLL::CreateNLL(%s)",_model.GetName())) ;

    pc.defineString("rangeName","RangeWithName",0,"",kTRUE) ;
    pc.defineString("addCoefRange","SumCoefRange",0,"") ;
    pc.defineString("globstag","GlobalObservablesTag",0,"") ;
    pc.defineDouble("rangeLo","Range",0,-999.) ;
    pc.defineDouble("rangeHi","Range",1,-999.) ;
    pc.defineInt("splitRange","SplitRange",0,0) ;
    pc.defineInt("ext","Extended",0,2) ;
    pc.defineInt("numcpu","NumCPU",0,1) ;
    pc.defineInt("interleave","NumCPU",1,0) ;
    pc.defineInt("verbose","Verbose",0,0) ;
    pc.defineInt("optConst","Optimize",0,0) ;
    pc.defineInt("cloneData","CloneData",2,0) ;
    pc.defineSet("projDepSet","ProjectedObservables",0,0) ;
    pc.defineSet("cPars","Constrain",0,0) ;
    pc.defineSet("glObs","GlobalObservables",0,0) ;
    pc.defineInt("constrAll","Constrained",0,0) ;
    pc.defineInt("doOffset","OffsetLikelihood",0,0) ;
    pc.defineSet("extCons","ExternalConstraints",0,0) ;
    pc.defineMutex("Range","RangeWithName") ;
    pc.defineMutex("Constrain","Constrained") ;
    pc.defineMutex("GlobalObservables","GlobalObservablesTag") ;

    // Process and check varargs 
    pc.process(cmdList) ;
    if (!pc.ok(kTRUE)) {
        return 0 ;
    }


    // Decode command line arguments
    const char* rangeName = pc.getString("rangeName",0,kTRUE) ;
    const char* addCoefRangeName = pc.getString("addCoefRange",0,kTRUE) ;
    const char* globsTag = pc.getString("globstag",0,kTRUE) ;
    Int_t ext      = pc.getInt("ext") ;
    Int_t numcpu   = pc.getInt("numcpu") ;
    RooFit::MPSplit interl = (RooFit::MPSplit) pc.getInt("interleave") ;

    Int_t splitr    = pc.getInt("splitRange") ;
    Bool_t verbose  = pc.getInt("verbose") ;
    Int_t optConst  = pc.getInt("optConst") ;
    Int_t cloneData = pc.getInt("cloneData") ;
    Int_t doOffset  = pc.getInt("doOffset") ;

    // If no explicit cloneData command is specified, cloneData is set to true if optimization is activated
    if (cloneData==2) {
        cloneData = optConst ;
    }

    RooArgSet* cPars = pc.getSet("cPars") ;
    RooArgSet* glObs = pc.getSet("glObs") ;
    if (pc.hasProcessed("GlobalObservablesTag")) {
    if (glObs) delete glObs ;
        RooArgSet* allVars = _model.getVariables() ;
        glObs = (RooArgSet*) allVars->selectByAttrib(globsTag,kTRUE) ;
        RooMsgService::instance().log(&_model,RooFit::INFO,RooFit::Minimization) << "User-defined specification of global observables definition with tag named '" <<  globsTag << "'" << std::endl ;
        delete allVars ;
    } else if (!pc.hasProcessed("GlobalObservables")) {

        // Neither GlobalObservables nor GlobalObservablesTag has been processed - try if a default tag is defined in the head node
        // Check if head not specifies default global observable tag
        const char* defGlobObsTag = _model.getStringAttribute("DefaultGlobalObservablesTag") ;
        if (defGlobObsTag) {
            RooMsgService::instance().log(&_model,RooFit::INFO,RooFit::Minimization) << "p.d.f. provides built-in specification of global observables definition with tag named '" <<  defGlobObsTag << "'" << std::endl ;
              if (glObs) delete glObs ;
              RooArgSet* allVars = _model.getVariables() ;
              glObs = (RooArgSet*) allVars->selectByAttrib(defGlobObsTag,kTRUE) ;
        }
    }

    
    Bool_t doStripDisconnected=kFALSE ;

    // If no explicit list of parameters to be constrained is specified apply default algorithm
    // All terms of RooProdPdfs that do not contain observables and share a parameters with one or more
    // terms that do contain observables are added as constraints.
    if (!cPars) {    
        cPars = _model.getParameters(_binnedData,kFALSE) ;
        doStripDisconnected=kTRUE ;
    }
    const RooArgSet* extCons = pc.getSet("extCons") ;

    // Process automatic extended option
    if (ext==2) {
        ext = ((_model.extendMode()==RooAbsPdf::ExtendMode::CanBeExtended || _model.extendMode()==RooAbsPdf::ExtendMode::MustBeExtended)) ? 1 : 0 ;
        if (ext) {
            RooMsgService::instance().log(&_model,RooFit::INFO,RooFit::Minimization) << "p.d.f. provides expected number of events, including extended term in likelihood." << std::endl ;
        }
    }

    if (pc.hasProcessed("Range")) {
        Double_t rangeLo = pc.getDouble("rangeLo") ;
        Double_t rangeHi = pc.getDouble("rangeHi") ;

        // Create range with name 'fit' with above limits on all observables
        RooArgSet* obs = _model.getObservables(&_binnedData) ;
        TIterator* iter = obs->createIterator() ;
        RooAbsArg* arg ;
        while((arg=(RooAbsArg*)iter->Next())) {
            RooRealVar* rrv =  dynamic_cast<RooRealVar*>(arg) ;
            if (rrv) rrv->setRange("fit",rangeLo,rangeHi) ;
        }
        // Set range name to be fitted to "fit"
        rangeName = "fit" ;
    }


    RooArgSet projDeps ;
    RooArgSet* tmp = pc.getSet("projDepSet") ;  
    if (tmp) {
        projDeps.add(*tmp) ;
    }

    // Construct NLL
    RooAbsReal::setEvalErrorLoggingMode(RooAbsReal::CollectErrors) ;
    RooAbsReal* nll ;
    std::string baseName = Form("nll_%s_%s",_model.GetName(),_binnedData.GetName()) ;
    if (!rangeName || strchr(rangeName,',')==0) {
        // Simple case: default range, or single restricted range
        //cout<<"FK: Data test 1: "<<data.sumEntries()<<std::endl;
        nll = new AdaptivegGKNLL(baseName.c_str(),"-log(likelihood)", _model,_binnedData,observable,
                                 projDeps,ext,rangeName,addCoefRangeName,numcpu,interl,verbose,splitr,cloneData);
        // nll = new RooNLLVar(baseName.c_str(),"-log(likelihood)",_model,_binnedData,projDeps,ext,rangeName,addCoefRangeName,numcpu,interl,verbose,splitr,cloneData) ;
    } else {
        // Composite case: multiple ranges
        RooArgList nllList ;
        const size_t bufSize = strlen(rangeName)+1;
        char* buf = new char[bufSize] ;
        strlcpy(buf,rangeName,bufSize) ;
        char* token = strtok(buf,",") ;
        while(token) {
            RooAbsReal * nllComp;
            nllComp = new AdaptivegGKNLL(Form("%s_%s",baseName.c_str(),token),"-log(likelihood)",_model,_binnedData,observable,
                                         projDeps,ext,rangeName,addCoefRangeName,numcpu,interl,verbose,splitr,cloneData);
            nllList.add(*nllComp) ;
            token = strtok(0,",") ;
        }
        delete[] buf ;
        nll = new RooAddition(baseName.c_str(),"-log(likelihood)",nllList,kTRUE) ;
    }
    RooAbsReal::setEvalErrorLoggingMode(RooAbsReal::PrintErrors) ;
  

    // Collect internal and external constraint specifications
    RooArgSet allConstraints ;
     if (cPars && cPars->getSize() > 0) {
        RooArgSet *constraints = getAllConstraints(_model, *_binnedData.get(), *cPars, doStripDisconnected);
        allConstraints.add(*constraints);
        delete constraints;
     }
     if (extCons) {
        allConstraints.add(*extCons);
     }

  // Include constraints, if any, in likelihood
    RooAbsReal* nllCons(0) ;
    if (allConstraints.getSize()>0 && cPars) {   

        RooMsgService::instance().log(&_model,RooFit::INFO,RooFit::Minimization) << " Including the following contraint terms in minimization: " << allConstraints << std::endl ;
        if (glObs) {
            RooMsgService::instance().log(&_model,RooFit::INFO,RooFit::Minimization) << "The following global observables have been defined: " << *glObs << std::endl ;
        }
        nllCons = new RooConstraintSum(Form("%s_constr",baseName.c_str()),"nllCons",allConstraints,glObs ? *glObs : *cPars) ;
        nllCons->setOperMode(_model.operMode()) ;
        RooAbsReal* orignll = nll ;

        nll = new RooAddition(Form("%s_with_constr",baseName.c_str()),"nllWithCons",RooArgSet(*nll,*nllCons)) ;
        nll->addOwnedComponents(RooArgSet(*orignll,*nllCons)) ;
    }

    if (optConst) {
        nll->constOptimizeTestStatistic(RooAbsArg::Activate,optConst>1) ;
    }

    if (doStripDisconnected) {
        delete cPars ;
    }

    if (doOffset) {
        nll->enableOffsetting(kTRUE) ;
    }

    return nll ;
}

RooAbsReal* BinnedAdaptivegGKNLL::CreateNLL(RooAbsPdf& _model, RooDataHist& _binnedData, RooRealVar& observable, const RooCmdArg& arg1, const RooCmdArg& arg2, const RooCmdArg& arg3, const RooCmdArg& arg4, 
                                             const RooCmdArg& arg5, const RooCmdArg& arg6, const RooCmdArg& arg7, const RooCmdArg& arg8) 
{
  RooLinkedList l ;
  l.Add((TObject*)&arg1) ;  l.Add((TObject*)&arg2) ;  
  l.Add((TObject*)&arg3) ;  l.Add((TObject*)&arg4) ;
  l.Add((TObject*)&arg5) ;  l.Add((TObject*)&arg6) ;  
  l.Add((TObject*)&arg7) ;  l.Add((TObject*)&arg8) ;
  return CreateNLL(_model, _binnedData, observable, l) ;
}

RooArgSet* BinnedAdaptivegGKNLL::getAllConstraints(RooAbsPdf& model, const RooArgSet& observables, RooArgSet& constrainedParams, Bool_t stripDisconnected) {
    RooArgSet* ret = new RooArgSet("AllConstraints") ;
    std::unique_ptr<RooArgSet> comps(model.getComponents());
    for (const auto arg : *comps) {
        auto pdf = dynamic_cast<const RooAbsPdf*>(arg) ;
        if (pdf && !ret->find(pdf->GetName())) {
            std::unique_ptr<RooArgSet> compRet(pdf->getConstraints(observables,constrainedParams,stripDisconnected));
            if (compRet) {
                ret->add(*compRet,kFALSE) ;
            }
        }
    }
    return ret ;
}


AdaptivegGKNLL::AdaptivegGKNLL() : RooNLLVar() {
}


AdaptivegGKNLL::AdaptivegGKNLL(const char *name, const char* title, RooAbsPdf& pdf, RooDataHist& data, RooRealVar& observable,
                             const RooCmdArg& arg1, const RooCmdArg& arg2,const RooCmdArg& arg3,
                             const RooCmdArg& arg4, const RooCmdArg& arg5,const RooCmdArg& arg6,
                             const RooCmdArg& arg7, const RooCmdArg& arg8,const RooCmdArg& arg9) :
                   RooNLLVar(name, title, pdf, data, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9),
                   m_first(true) {
    SetObservable(observable);
    StoreBinning();
    InitGSLWorkspace();
    InitGSLFunction();

    RooCmdConfig pc("RooNLLVar::RooNLLVar") ;
    pc.allowUndefined() ;
    pc.defineInt("extended","Extended",0,kFALSE) ;

    pc.process(arg1) ;  pc.process(arg2) ;  pc.process(arg3) ;
    pc.process(arg4) ;  pc.process(arg5) ;  pc.process(arg6) ;
    pc.process(arg7) ;  pc.process(arg8) ;  pc.process(arg9) ;

    m_extended = pc.getInt("extended") ;
}

AdaptivegGKNLL::AdaptivegGKNLL(const char *name, const char *title, RooAbsPdf& pdf, RooDataHist& data, RooRealVar& observable,
                             Bool_t extended, const char* rangeName, const char* addCoefRangeName, 
                             Int_t nCPU, RooFit::MPSplit interleave, Bool_t verbose, 
                             Bool_t splitRange, Bool_t cloneData, Bool_t binnedL) :
                   RooNLLVar(name, title, pdf, data, extended, rangeName, addCoefRangeName,
                             nCPU, interleave, verbose, splitRange, cloneData, binnedL),
                   m_first(true),
                   m_extended(extended) {
    SetObservable(observable);
    StoreBinning();
    InitGSLWorkspace();
    InitGSLFunction();
}
  
AdaptivegGKNLL::AdaptivegGKNLL(const char *name, const char *title, RooAbsPdf& pdf, RooDataHist& data, RooRealVar& observable,
                             const RooArgSet& projDeps, Bool_t extended, const char* rangeName, 
                             const char* addCoefRangeName, Int_t nCPU, RooFit::MPSplit interleave, Bool_t verbose, 
                             Bool_t splitRange, Bool_t cloneData, Bool_t binnedL) : 
                   RooNLLVar(name, title, pdf, data, projDeps, extended, rangeName, addCoefRangeName, 
                             nCPU, interleave, verbose, splitRange, cloneData, binnedL),
                   m_first(true),
                   m_extended(extended) {
    SetObservable(observable);
    StoreBinning();
    InitGSLWorkspace();
    InitGSLFunction();
}

AdaptivegGKNLL::AdaptivegGKNLL(const AdaptivegGKNLL& other, const char* name) : 
                   RooNLLVar(other, name), 
                   binBoundaries(other.binBoundaries), 
                   m_observable(other.m_observable) {
    InitGSLWorkspace();
    InitGSLFunction();
}

AdaptivegGKNLL::~AdaptivegGKNLL(){
        gsl_integration_workspace_free(m_integrationWorkspace);
}

void AdaptivegGKNLL::SetObservable(RooRealVar& observable){
    auto * pdf = (RooAbsPdf*)_funcClone;
    auto * variables = pdf->getVariables();
    auto & clonedObservable = (*variables)[observable.GetName()];
    m_observable = (RooRealVar*)&clonedObservable;
    delete variables;
}

void AdaptivegGKNLL::StoreBinning(){
    const auto& binning = m_observable->getBinning();
    size_t nBins = binning.numBins();
    binBoundaries.reserve(nBins+1);
    for (size_t i = 0; i < nBins; i++){
        binBoundaries.push_back(binning.binLow(i));
    }
    binBoundaries.push_back(binning.binHigh(nBins-1));
}

void AdaptivegGKNLL::InitGSLWorkspace(){
    m_integrationWorkspace = gsl_integration_workspace_alloc(1000);
}

void AdaptivegGKNLL::InitGSLFunction(){
    m_gslFunction.function = &BinIntegrator::evaluatePDF;
    m_gslParams.push_back((void*)m_observable);
    m_gslParams.push_back((void*)_funcClone);
    m_gslParams.push_back((void*)_normSet);
    // m_gslParams.push_back((void*)&m_gslNormalisation);
    m_gslFunction.params = &m_gslParams[0];
}

Double_t AdaptivegGKNLL::evaluatePartition(std::size_t firstEvent, std::size_t lastEvent, std::size_t stepSize) const{
    // First Implementation without Kahan's algorithm
    RooAbsPdf* pdfClone = (RooAbsPdf*) _funcClone ;
    _dataClone->store()->recalculateCache(_projDeps, firstEvent, lastEvent, stepSize, true) ;
    size_t nBins = (lastEvent - firstEvent)/stepSize;
    m_mu.reserve(nBins);
    m_n.reserve(nBins);

    double integral, abserr, result(0), carry(0);
    size_t nEval;

    for (auto i=firstEvent; i<lastEvent; i+=stepSize) {
        _dataClone->get(i);
        m_n.push_back(_dataClone->weight());
        double _binLow = binBoundaries[i];
        double _binHigh = binBoundaries[i+1];
        // m_gslNormalisation = pdfClone->getVal(_normSet)/pdfClone->getVal();
        gsl_integration_qag(&m_gslFunction, _binLow, _binHigh, 0, 1e-7, 1000, 1, m_integrationWorkspace, &integral, &abserr);
        m_mu.push_back(integral);
    }

    // Kahan summation of likelihoods
    for (size_t i=0; i<m_mu.size(); i++){
        double y = masked_likelihood(m_mu[i],m_n[i]) - carry;
        double t = result + y;
        carry = (t - result) - y;
        result = t;
    }

    // include the extended maximum likelihood term, if requested
    if(m_extended && _setNum==_extSet) {
        // _weightSq term is a private member of RooNLLVar in 6.20 and later
        // if (_weightSq) {
        //     // Calculate sum of weights-squared here for extended term
        //     double sumW2(0), sumW2carry(0);
        //     for (size_t i=0 ; i<_dataClone->numEntries() ; i++) {
        //         _dataClone->get(i);
        //         double y = _dataClone->weightSquared() - sumW2carry;
        //         double t = sumW2 + y;
        //         sumW2carry = (t - sumW2) - y;
        //         sumW2 = t;
        //     }
        //     double expected = pdfClone->expectedEvents(_dataClone->get());
        //     double expectedW2 = expected * sumW2 / _dataClone->sumEntries() ;
        //     double extra = expectedW2 - sumW2*log(expected);
        //     double y = extra - carry;

        //     double t = result + y;
        //     carry = (t - result) - y;
        //     result = t;
        // } 
        // else {
            double y = pdfClone->extendedTerm(_dataClone->sumEntries(), _dataClone->get()) - carry;
            double t = result + y;
            carry = (t - result) - y;
            result = t;
        // }
    }

    if (m_first) {
        m_first = false;
        _funcClone->wireAllCaches() ;
    }

  // Check if value offset flag is set.
    if (_doOffset) {

        // If no offset is stored enable this feature now
        if (_offset==0 && result !=0 ) {
            coutI(Minimization) << "AdaptivegGKNLL::evaluatePartition(" << GetName() << ") first = "<< firstEvent << " last = " << lastEvent << " Likelihood offset now set to " << result << std::endl ;
            _offset = result ;
            _offsetCarry = carry;
        }

        // Substract offset
        double y = -_offset - (carry + _offsetCarry);
        double t = result + y;
        carry = (t - result) - y;
        result = t;
    }
    m_n.clear();
    m_mu.clear();

    return result;
}

#endif
