#ifndef TF1FUNCTION_CPP
#define TF1FUNCTION_CPP

#include "TF1Function.h"
#include <cmath>

TF1Function::TF1Function(){
    m_xmin = 5000;
    m_xmax = 6000;
}

TF1Function::TF1Function(int nbins, double xmin, double xmax){
    SetNBins(nbins);
    SetMin(xmin);
    SetMax(xmax);
}

void TF1Function::SetNBins(int nbins){
    m_nbins = nbins;
}

void TF1Function::SetMin(double min){
    m_xmin = min;
}

void TF1Function::SetMax(double max){
    m_xmax = max;
}

double TF1Function::operator() (double *x, double *pars) const {
    double width = (m_xmax - m_xmin) / m_nbins;
    double DSCBNorm = GetNormalisationDSCB(pars[2], pars[3], pars[5], pars[6], pars[7], pars[8]);
    double expNorm = GetNormalisationExponential(pars[4]);
    double value = width*(
                    pars[0] * DSCBNorm * Compute_DSCB_PDF(x[0], pars[2], pars[3], pars[5], pars[6], pars[7], pars[8])
                    + pars[1] * expNorm * exp(pars[4]*x[0]));
    return value;
}

double TF1Function::GetNormalisationExponential(double slope) const {
    double unnormalizedIntegral = exp(slope*m_xmax) - exp(slope*m_xmin);
    double normalisation = slope/unnormalizedIntegral;
    return normalisation;
}

double TF1Function::Compute_DSCB_PDF(double x, double mean, double sigma,
                                     double alphaLow, double nLow, 
                                     double alphaHigh, double nHigh) const {
    double t      = (x - mean) / sigma;
    double result = 0;
    if (t < -alphaLow) {
        double a = pow(nLow / alphaLow, nLow) * exp(-0.5 * alphaLow * alphaLow);
        double b = nLow / alphaLow - alphaLow;
        result   = a / pow(b - t, nLow);
    } else if (t >= -alphaLow && t < alphaHigh) {
        result = exp(-0.5 * t * t);
    } else if (t >= alphaHigh) {
        double a = pow(nHigh / alphaHigh, nHigh) * exp(-0.5 * alphaHigh * alphaHigh);
        double b = nHigh / alphaHigh - alphaHigh;
        result   = a / pow(b + t, nHigh);
    }
    return result;
}

// We force 
double TF1Function::GetNormalisationDSCB(double mean, double sigma,
                                         double alphaLow, double nLow, 
                                         double alphaHigh, double nHigh) const {
    double xMinCDF = Compute_DSCB_CDF(m_xmin, mean, sigma, alphaLow, nLow, alphaHigh, nHigh);
    double xMaxCDF = Compute_DSCB_CDF(m_xmax, mean, sigma, alphaLow, nLow, alphaHigh, nHigh);
    return 1./(xMaxCDF - xMinCDF);
};

double TF1Function::Compute_DSCB_CDF(double x, double mean, double sigma,
                                     double alphaLow, double nLow, 
                                     double alphaHigh, double nHigh) const {
    double t = (x - mean)/sigma;
    double result;
    double aLow = pow(nLow / alphaLow, nLow) * exp(-0.5 * alphaLow * alphaLow);
    double bLow = nLow / alphaLow - alphaLow;
    // Low tail region
    if (t <= -alphaLow) {
        bool useLog = (abs(nLow - 1.0) < 1.0e-5);
        if (useLog) {
            result = -1. * aLow * sigma * log(bLow - t);
        } else {
            result = (aLow * sigma / (nLow - 1)) / pow(bLow - t, nLow - 1);
        }
    } else if (t > -alphaLow && t <= alphaHigh) {
        bool lowUseLog = (abs(nLow - 1.0) < 1.0e-5);
        // Integral of lower tail
        if (lowUseLog) {
            result = -1. * aLow * sigma * log(nLow / alphaLow);
        } else {
            result = (aLow * sigma / (nLow - 1)) / pow(nLow / alphaLow, nLow - 1);
        }
        result += sigma * sqrtPi * invSqrt2 * (erf(t * invSqrt2) + erf(alphaLow * invSqrt2));
    } else {
        bool   lowUseLog  = (abs(nLow - 1.0) < 1.0e-5);
        bool   highUseLog = (abs(nHigh - 1.0) < 1.0e-5);
        double aHigh      = pow(nHigh / alphaHigh, nHigh) * exp(-0.5 * alphaHigh * alphaHigh);
        double bHigh      = nHigh / alphaHigh - alphaHigh;
        // Integral of lower tail
        if (lowUseLog) {
            result = -1. * aLow * sigma * log(nLow / alphaLow);
        } else {
            result = (aLow * sigma / (nLow - 1)) / pow(nLow / alphaLow, nLow - 1);
        }
        // Integral of Gaussian core
        result += sigma * sqrtPi * invSqrt2 * (erf(alphaHigh * invSqrt2) + erf(alphaLow * invSqrt2));
        if (highUseLog) {
            result += aHigh * sigma * (log(bHigh + t) - log(nHigh / alphaHigh));
        } else {
            result += (aHigh * sigma / (1. - nHigh)) * (1. / pow(bHigh + t, nHigh - 1.) - 1. / pow(nHigh / alphaHigh, nHigh - 1.));
        }
    }
    return result;
}

#endif
