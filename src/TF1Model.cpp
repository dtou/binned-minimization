#ifndef TF1MODEL_CPP
#define TF1MODEL_CPP

#include "TF1Model.h"
#include "RooAbsBinning.h"

TF1Model::TF1Model() : m_function() {
    m_model = nullptr;
}

TF1Model::TF1Model(const RooRealVar& observable) : TF1Model() {
    SetRange(observable);
}

void TF1Model::SetRange(const RooRealVar& observable){
    DeleteTF1IfNotNull();
    m_function.SetNBins(observable.getBinning().numBins());
    m_function.SetMin(observable.getMin());
    m_function.SetMax(observable.getMax());
    m_model = new TF1("function", m_function, observable.getMin(), observable.getMax(), 9);
    m_model->SetParNames("nSignal", "nBackground", 
                         "mean", "sigma", "slope",
                         "aLow", "nLow", "aHigh", "nHigh");
}

void TF1Model::DeleteTF1IfNotNull(){
    if (m_model != nullptr){
        delete m_model;
        m_model = nullptr;
    }
}

void TF1Model::ResetVariables(){
    // Floating
    m_model->SetParameter(0, m_initialNSig);
    m_model->SetParameter(1, m_initialNBkg);
    m_model->SetParameter(2, m_initialMu);
    m_model->SetParameter(3, m_initialSigma);
    m_model->SetParameter(4, m_initialSlope);
    // Floating error (step size)
    m_model->SetParError(0, 0.05*m_initialNSig);
    m_model->SetParError(1, 0.05*m_initialNBkg);
    m_model->SetParError(2, 0.05*m_initialMu);
    m_model->SetParError(3, 0.05*m_initialSigma);
    m_model->SetParError(4, 0.05*m_initialSlope);
    // Fixed
    m_model->FixParameter(5, m_aLow);
    m_model->FixParameter(6, m_nLow);
    m_model->FixParameter(7, m_aHigh);
    m_model->FixParameter(8, m_nHigh);
}

void TF1Model::ConfigureSignal(int n_signal, double mean, double sigma,
                     double aLow, double nLow, double aHigh, double nHigh){
    m_initialNSig = n_signal;
    m_initialMu = mean;
    m_initialSigma = sigma;
    m_aLow = aLow;
    m_nLow = nLow;
    m_aHigh = aHigh;
    m_nHigh = nHigh;
}

void TF1Model::ConfigureBackground(int n_background, double slope){
    m_initialNBkg = n_background;
    m_initialSlope = slope;
}

TF1 * TF1Model::GetTF1Model(){
    return m_model;
};

TF1Model::~TF1Model(){
    DeleteTF1IfNotNull();
}

#endif
