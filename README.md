# Standalone test of bias in binned fits
This is intended as a standalone test to study the bias in binned fits using RooFit.

## Minimum requirements
Either access to cvmfs or
1. cmake
2. ROOT

## ROOT version
Note that there are two tags, one tested for ROOT v6.20 and another tested for ROOT v6.18. They are meant for ROOT versions v6.20 and later or ROOT v6.18 and earlier. If you find that they are not working properly, please send me an email.

## Installation
Once you have cmake, all you need to do is
```bash
mkdir build
cd build
cmake .. (add -G Ninja to compile with ninja)
make (or ninja) -j <number of cores you want to burn>
```
The executables compiled to `bin`.

## Model
The model uses a double-sided Crystal Ball (DSCB) function as signal and an exponential function as the background. The toy fit is minimized using the extended likelihood formula. During the toy fits the following are fixed:
- Lower and upper DSCB tails `alpha`
- Lower and upper DSCB tails `n`

while the following are left floating:
- DSCB Gaussian core mean
- DSCB Gaussian core width
- Exponential background slope
- Signal yield
- Background yield

## Using the code for my analysis
If you use RooFit's `RooAbsPdf::fitTo()` then you need to modify your code to pass the likelihood objects and minimize it using `RooMinimizer` (you can consult `Minimizer.cpp`). If you are familiar with `RooAbsPdf::createNLL()` and minimize the likelihood object then you only need to replace the `RooAbsPdf::createNLL()` call with one of the `CreateNLL()` functions provided here.

There are 3 choices of numerical integration:
1. Romberg (this is the fastest and I use this in my analysis) in `RombergNLL.h`
2. Iterative Gauss-Kronrod (GSL QNG numerical integration) in `IterativeGKNLL.h`
3. Adaptive Gauss-Kronrod (GSL QAG numerical integration) in `AdaptivegGKNLL.h`

You only need to copy-paste one header file and the corresponding source file to implement them in your analysis code.

## Mathematical limitations
- These per-bin integrals are only valid for 1D binned fits.
- One assumption of these integrands is that there are no integrable singularities within your model. I would be surprised if you have one but we can work out a solution using GSL's other numerical integration.

## Questions?
You can contact me via `dtou@cern.ch` if you have further questions!
