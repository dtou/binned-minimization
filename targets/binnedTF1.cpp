#ifndef BINNEDTF1_CPP
#define BINNEDTF1_CPP

#include "cxxopts.hpp"
#include <iostream>
#include <string>
#include "Generator.h"
#include "TF1Model.h"
#include "TF1ResultLogger.h"
#include "TH1.h"
#include "TFitResultPtr.h"

#include <iostream>

int main(int argc, char * argv[]){
    cxxopts::Options options("truth", "Signal: double-sided crystall ball (DSCB); Background : exponential");
    
    options.add_options("generator truths")
        ("gm", "'mu' parameter in generator DSCB.", cxxopts::value<double>()->default_value("5428.65"))
        ("gsi", "'sigma' parameter in generator DSCB.", cxxopts::value<double>()->default_value("20"))
        ("gsl", "Slope in generator exponential background.", cxxopts::value<double>()->default_value("-0.002"))
        ("gnsig", "Mean number of generator signal yields.", cxxopts::value<int>()->default_value("1000000"))
        ("gnbkg", "Mean number of generator background yields.", cxxopts::value<int>()->default_value("150000"));

    options.add_options("fixed parameters")
        ("al", "The lower tail 'alpha' parameter used by generator and toy fit DSCB.", cxxopts::value<double>()->default_value("1.5"))
        ("nl", "The lower tail 'n' parameter used by generator and toy fit DSCB.", cxxopts::value<double>()->default_value("3"))
        ("ah", "The upper tail 'alpha' parameter used by generator and toy fit DSCB.", cxxopts::value<double>()->default_value("2"))
        ("nh", "The upper tail 'n' parameter used by generator and toy fit DSCB.", cxxopts::value<double>()->default_value("4"));

    options.add_options("toy fit initial values")
        ("im", "The initial 'mu' parameter in signal DSCB toy fit model.", cxxopts::value<double>()->default_value("5430"))
        ("isi", "The initial 'sigma' parameter in signal DSCB toy fit model.", cxxopts::value<double>()->default_value("20"))
        ("isl", "The initial exponential background slope in toy fit model.", cxxopts::value<double>()->default_value("-0.002"))
        ("insig", "The initial signal yield of toy fit value model.", cxxopts::value<int>()->default_value("1000000"))
        ("inbkg", "The initial background yield of toy fit model.", cxxopts::value<int>()->default_value("150000"));

    options.add_options("Other toy generator and fit options")
        ("nBins", "The number of bins used in plotting and binned fits.", cxxopts::value<unsigned int>()->default_value("600"))
        ("seed", "The generator seed to use.", cxxopts::value<unsigned long int>()->default_value("0"))
        ("nToys", "The number of toys to generate", cxxopts::value<unsigned int>()->default_value("1000"))
        ("i,integral", "Use the integral instead of function at bin center", cxxopts::value<bool>()->default_value("false")->implicit_value("true"));

    options.add_options("Output")
        ("o,outputfile", "The output file to store the results", cxxopts::value<std::string>()->default_value("results.root"));

    options.add_options()
        ("h,help", "Print usage");

    auto args = options.parse(argc, argv);

    if (args.count("help")){
      std::cout << options.help() << std::endl;
      exit(0);
    }

    double genMu = args["gm"].as<double>();
    double genSigma = args["gsi"].as<double>();
    double genSlope = args["gsl"].as<double>();
    int genNSig = args["gnsig"].as<int>();
    int genNBkg = args["gnbkg"].as<int>();

    double initMu = args["im"].as<double>();
    double initSigma = args["isi"].as<double>();
    double initSlope = args["isl"].as<double>();
    int initNSig = args["insig"].as<int>();
    int initNBkg = args["inbkg"].as<int>();

    double aLow = args["al"].as<double>();
    double nLow = args["nl"].as<double>();
    double aHigh = args["ah"].as<double>();
    double nHigh = args["nh"].as<double>();

    unsigned int nBins = args["nBins"].as<unsigned int>();
    unsigned long int seed = args["seed"].as<unsigned long int>();
    unsigned int nToys = args["nToys"].as<unsigned int>();
    bool useIntegral = args["integral"].as<bool>();

    std::string outputfile = args["outputfile"].as<std::string>();

    RooRealVar mass("mass", "mass", 5000, 6000);
    mass.setBins(nBins);
    Generator generator(mass, seed);
    generator.ConfigureSignal(genNSig, genMu, genSigma, aLow, nLow, aHigh, nHigh);
    generator.ConfigureBackground(genNBkg, genSlope);

    TF1Model model(mass);

    model.ConfigureSignal(initNSig, initMu, initSigma, aLow, nLow, aHigh, nHigh);
    model.ConfigureBackground(initNBkg, initSlope);

    TF1 * fitFunction = model.GetTF1Model();
    TF1ResultLogger logger;
    std::string fitOptions = useIntegral ? "MILOS" : "MLOS";

    for (int i = 0; i < nToys; i++){
        model.ResetVariables();
        auto dataset = generator.Generate();
        auto binned_TH1 = dataset->createHistogram("hist", mass, RooFit::Binning(nBins, mass.getMin(), mass.getMax()));
        auto result = binned_TH1->Fit(fitFunction, fitOptions.c_str(), "", mass.getMin(), mass.getMax());
        logger.LogFit(result);
        delete dataset;
        delete binned_TH1;
    }

    TString treename = "ToyFitResult";
    logger.SaveResults(outputfile.c_str(), treename);
    return 0;
}

#endif
