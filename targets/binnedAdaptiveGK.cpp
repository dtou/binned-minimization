#ifndef BINNEDADAPTIVEGK_CPP
#define BINNEDADAPTIVEGK_CPP

#include "cxxopts.hpp"
#include "Generator.h"
#include "VariableResetter.h"
#include "Model.h"
#include "FitResultLogger.h"
#include "Minimizer.h"
#include "RooDataHist.h"
#include "AdaptivegGKNLL.h"
#include "RooPlot.h"
#include "TCanvas.h"
#include "RooAbsData.h"
#include <chrono>
#include <fstream>
#include <iostream>

int main(int argc, char * argv[]){
    cxxopts::Options options("truth", "Signal: double-sided crystall ball (DSCB); Background : exponential");
    
    options.add_options("generator truths")
        ("gm", "'mu' parameter in generator DSCB.", cxxopts::value<double>()->default_value("5428.65"))
        ("gsi", "'sigma' parameter in generator DSCB.", cxxopts::value<double>()->default_value("20"))
        ("gsl", "Slope in generator exponential background.", cxxopts::value<double>()->default_value("-0.002"))
        ("gnsig", "Mean number of generator signal yields.", cxxopts::value<int>()->default_value("1000000"))
        ("gnbkg", "Mean number of generator background yields.", cxxopts::value<int>()->default_value("150000"));

    options.add_options("fixed parameters")
        ("al", "The lower tail 'alpha' parameter used by generator and toy fit DSCB.", cxxopts::value<double>()->default_value("1.5"))
        ("nl", "The lower tail 'n' parameter used by generator and toy fit DSCB.", cxxopts::value<double>()->default_value("3"))
        ("ah", "The upper tail 'alpha' parameter used by generator and toy fit DSCB.", cxxopts::value<double>()->default_value("2"))
        ("nh", "The upper tail 'n' parameter used by generator and toy fit DSCB.", cxxopts::value<double>()->default_value("4"));

    options.add_options("toy fit initial values")
        ("im", "The initial 'mu' parameter in signal DSCB toy fit model.", cxxopts::value<double>()->default_value("5430"))
        ("isi", "The initial 'sigma' parameter in signal DSCB toy fit model.", cxxopts::value<double>()->default_value("20"))
        ("isl", "The initial exponential background slope in toy fit model.", cxxopts::value<double>()->default_value("-0.002"))
        ("insig", "The initial signal yield of toy fit value model.", cxxopts::value<int>()->default_value("1000000"))
        ("inbkg", "The initial background yield of toy fit model.", cxxopts::value<int>()->default_value("150000"));

    options.add_options("Other toy generator and fit options")
        ("nBins", "The number of bins used in plotting and binned fits.", cxxopts::value<unsigned int>()->default_value("600"))
        ("seed", "The generator seed to use.", cxxopts::value<unsigned long int>()->default_value("0"))
        ("nToys", "The number of toys to generate", cxxopts::value<unsigned int>()->default_value("1000"));

    options.add_options("Output")
        ("o,outputfile", "The output file to store the results", cxxopts::value<std::string>()->default_value("results.root"));

    options.add_options()
        ("h,help", "Print usage");

    auto args = options.parse(argc, argv);

    if (args.count("help")){
      std::cout << options.help() << std::endl;
      exit(0);
    }

    double genMu = args["gm"].as<double>();
    double genSigma = args["gsi"].as<double>();
    double genSlope = args["gsl"].as<double>();
    int genNSig = args["gnsig"].as<int>();
    int genNBkg = args["gnbkg"].as<int>();

    double initMu = args["im"].as<double>();
    double initSigma = args["isi"].as<double>();
    double initSlope = args["isl"].as<double>();
    int initNSig = args["insig"].as<int>();
    int initNBkg = args["inbkg"].as<int>();

    double aLow = args["al"].as<double>();
    double nLow = args["nl"].as<double>();
    double aHigh = args["ah"].as<double>();
    double nHigh = args["nh"].as<double>();

    unsigned int nBins = args["nBins"].as<unsigned int>();
    unsigned long int seed = args["seed"].as<unsigned long int>();
    unsigned int nToys = args["nToys"].as<unsigned int>();

    std::string outputfile = args["outputfile"].as<std::string>();

    RooRealVar mass("mass", "mass", 5000, 6000);
    mass.setBins(nBins);
    Generator generator(mass, seed);
    generator.ConfigureSignal(genNSig, genMu, genSigma, aLow, nLow, aHigh, nHigh);
    generator.ConfigureBackground(genNBkg, genSlope);

    Model model(mass);
    model.ConfigureSignal(initNSig, initMu, initSigma, aLow, nLow, aHigh, nHigh);
    model.ConfigureBackground(initNBkg, initSlope);

    auto * pdf = model.GetPDF();

    VariableResetter resetter;
    FitResultLogger logger;
    Minimizer minimizer;
    resetter.AddVariablesFromModel(*pdf);
    logger.AddVariablesFromModel(*pdf);
    RooAbsData::setDefaultStorageType(RooAbsData::StorageType::Vector);
    // RooAbsReal * CreateNLL(RooAbsPdf& _model, RooDataHist& _binnedData, const RooRealVar& observable, const RooLinkedList& cmdList) const ;
    std::chrono::milliseconds totalDuration(0);

    for (int i = 0; i < nToys; i++){
        resetter.ResetAllVariables();
        auto dataset = generator.Generate();
        auto binned_dataset = RooDataHist("hist", "hist", RooArgList(mass), *dataset);
        auto start = std::chrono::steady_clock::now();
        auto * logLikelihood = BinnedAdaptivegGKNLL::CreateNLL(*pdf, binned_dataset, mass, RooFit::Extended(1), RooFit::Offset(1));
        minimizer.minimize(logLikelihood);
        auto end = std::chrono::steady_clock::now();
        auto * result = minimizer.GetRooFitResult();
        logger.LogFit(*result);
        auto duration = std::chrono::duration_cast<std::chrono::milliseconds> (end - start);
        totalDuration += duration;
        delete logLikelihood;
        delete dataset;
        minimizer.DeleteResultIfNotNull();
    }

    TString treename = "ToyFitResult";
    logger.SaveResults(outputfile.c_str(), treename);

    std::chrono::duration<float, std::ratio<1, 1>> totalSeconds(totalDuration);
    std::fstream file;
    file.open("time.txt", std::fstream::in | std::fstream::out | std::fstream::app);
    file << "nToys : " << nToys << std::endl;
    file << "Time  : " << totalSeconds.count() << std::endl;
    file.close();

    return 0;
}

#endif
