#ifndef PULLS_CPP
#define PULLS_CPP

#include "cxxopts.hpp"
#include "RooRealVar.h"
#include "RooPlot.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "TLine.h"
#include "TCanvas.h"
#include "TPaveText.h"
#include "ROOT/RDataFrame.hxx"

#include <tuple>
#include <string>
#include <vector>
#include <map>
#include <fstream>
#include <iostream>


TPaveText * WriteText(const RooRealVar * mean, const RooRealVar * sigma){
    char buffer[50];
    double mean_value = mean->getVal();
    double sigma_value = sigma->getVal();
    double mean_error = mean->getError();
    double sigma_error = sigma->getError();
    auto text = new TPaveText(.1, .7, .4, .9, "NDC");
    sprintf(buffer, "Mean  = %.4f +/- %.4f", mean_value, mean_error);
    text->AddText(buffer);
    sprintf(buffer, "Sigma  = %.3f +/- %.3f", mean_value, mean_error);
    text->AddText(buffer);
    return text;
}

TLine * DrawLine(double y_max){
    auto line = new TLine(0, 0, 0, y_max);
    line->SetLineColorAlpha(921, 0.5);
    line->SetLineWidth(10);
    return line;
}

std::map<std::string, std::vector<double>> GetPulls(TTree * resultTree, double muTruth, double sigmaTruth,
                                                    double slopeTruth, int nsigTruth, int nbkgTruth){
    ROOT::RDataFrame dataframe(*resultTree);
    auto selection = [](int fitStatus, int covQuality){ return (fitStatus == 0 && covQuality == 3); };
    auto filtered = dataframe.Filter(selection, {"fitStatus", "covarianceQuality"});

    auto computeMuPull = [&muTruth](double mu, double muError){ return (mu-muTruth)/muError; };
    auto computeSigmaPull = [&sigmaTruth](double sigma, double sigmaError){ return (sigma-sigmaTruth)/sigmaError; };
    auto computeSlopePull = [&slopeTruth](double slope, double slopeError){ return (slope-slopeTruth)/slopeError; };
    auto computeNSigPull = [&nsigTruth](double nsig, double nsigError){ return (nsig-nsigTruth)/nsigError; };
    auto computeNBkgPull = [&nbkgTruth](double nbkg, double nbkgError){ return (nbkg-nbkgTruth)/nbkgError; };

    auto pullsDataframe = filtered.Define("mu_pull", computeMuPull, {"mean", "mean_error"})
                                  .Define("sigma_pull", computeSigmaPull, {"sigma", "sigma_error"})
                                  .Define("slope_pull", computeSlopePull, {"slope", "slope_error"})
                                  .Define("nsig_pull", computeNSigPull, {"nSignal", "nSignal_error"})
                                  .Define("nbkg_pull", computeNBkgPull, {"nBackground", "nBackground_error"});

    auto muPulls = pullsDataframe.Take<double>("mu_pull");
    auto sigmaPulls = pullsDataframe.Take<double>("sigma_pull");
    auto slopePulls = pullsDataframe.Take<double>("slope_pull");
    auto nsigPulls = pullsDataframe.Take<double>("nsig_pull");
    auto nbkgPulls = pullsDataframe.Take<double>("nbkg_pull");

    std::map<std::string, std::vector<double>> pulls = 
    { {"mu", muPulls.GetValue()},
      {"sigma", sigmaPulls.GetValue()},
      {"slope", slopePulls.GetValue()},
      {"nsig", nsigPulls.GetValue()},
      {"nbkg", nbkgPulls.GetValue()}};
    return std::move(pulls);
}

std::tuple<RooRealVar*, RooRealVar*, RooPlot*> FitPulls(const std::string & label, const std::vector<double> & pulls){
    RooRealVar pull("pull", "pull", -5, 5);
    RooArgSet set(pull);
    RooDataSet dataset("dataset", "dataset", set);
    for (const auto& value : pulls){
        pull.setVal(value);
        dataset.add(set);
    }

    RooRealVar * mean = new RooRealVar("mean", "mean", 0, -10, 10);
    RooRealVar * sigma = new RooRealVar("sigma", "sigma", 1, 0, 10);
    RooGaussian gauss("gauss", "gauss", pull, *mean, *sigma);
    gauss.fitTo(dataset);
    auto plot = pull.frame(RooFit::Bins(50), RooFit::Title(label.c_str()));
    dataset.plotOn(plot, RooFit::MarkerStyle(2), RooFit::MarkerColor(601), RooFit::LineColor(601));
    gauss.plotOn(plot, RooFit::LineColor(kRed), RooFit::LineWidth(2));
    return std::make_tuple(mean, sigma, plot);
}

void DrawPlot(const RooRealVar * mean, const RooRealVar * sigma, RooPlot * plot, const std::string & outputname){
    auto text = WriteText(mean, sigma);
    auto line = DrawLine(plot->GetMaximum());
    TCanvas c("c", "c", 800, 600);
    plot->Draw();
    text->Draw();
    line->Draw();
    c.SaveAs(outputname.c_str());
}

void DumpPulls(std::map <std::string, RooRealVar*> means,
               std::map <std::string, RooRealVar*> sigmas,
               std::string outputfile){
    std::fstream file;
    char buffer[128];
    file.open(outputfile.c_str(), std::fstream::in | std::fstream::out | std::fstream::trunc);
    file << "Parameter\t" << " mean   +/- error \t" << "sigma +/- error\n";
    file << "---------\t" << "------------------\t" << "---------------\n";
    std::string format =  "%s\t\t%.4f +/- %.4f\t%.3f +/- %.3f\n";
    for (auto& pair : means){
        const auto& key = pair.first;
        auto mean = means[key];
        auto sigma = sigmas[key];
        sprintf(buffer, format.c_str(), key.c_str(), mean->getVal(), mean->getError(), sigma->getVal(), sigma->getError());
        file << buffer;
    }
    file.close();
}


int main(int argc, char * argv[]){
    cxxopts::Options options("truth", "Signal: double-sided crystall ball (DSCB); Background : exponential");
    
    options.add_options("Input")
        ("i,inputfile", "Path to toy fit result nTuple.", cxxopts::value<std::string>()->default_value("results.root"));

    options.add_options("Output")
        ("e,extension", "The output extension to plot the pulls. 'pdf'(default) or 'png'", cxxopts::value<std::string>()->default_value("pdf"))
        ("o,outputfile", "The output file to store the pulls", cxxopts::value<std::string>()->default_value("pulls.log"));

    options.add_options("generator truths")
        ("mu", "'mu' parameter in generator DSCB.", cxxopts::value<double>()->default_value("5428.65"))
        ("sigma", "'sigma' parameter in generator DSCB.", cxxopts::value<double>()->default_value("20"))
        ("slope", "Slope in generator exponential background.", cxxopts::value<double>()->default_value("-0.002"))
        ("nsig", "Mean number of generator signal yields.", cxxopts::value<int>()->default_value("1000000"))
        ("nbkg", "Mean number of generator background yields.", cxxopts::value<int>()->default_value("150000"));

    options.add_options()
        ("h,help", "Print usage");

    auto args = options.parse(argc, argv);

    if (args.count("help")){
      std::cout << options.help() << std::endl;
      exit(0);
    }

    std::string inputfile = args["inputfile"].as<std::string>();
    TFile resultNTuple(inputfile.c_str(), "READ");
    if (resultNTuple.IsZombie()){
        std::cout << "--inputfile parsed cannot be opened as a TFile!" << std::endl;
    }
    auto resultTree = resultNTuple.Get<TTree>("ToyFitResult");
    if (resultTree == nullptr){
        std::cout << "--inputfile parsed does not contain a ToyFitResult TTree" << std::endl;
    }

    std::string extension = args["extension"].as<std::string>();
    if ((extension != "png") && (extension != "pdf")){
        std::cout << "--extension only accepts 'pdf' or 'png'" << std::endl;
        exit(0);
    }

    double muTruth = args["mu"].as<double>();
    double sigmaTruth = args["sigma"].as<double>();
    double slopeTruth = args["slope"].as<double>();
    int nsigTruth = args["nsig"].as<int>();
    int nbkgTruth = args["nbkg"].as<int>();

    std::string outputfile = args["outputfile"].as<std::string>();

    std::map <std::string, RooRealVar*> means;
    std::map <std::string, RooRealVar*> sigmas;
    auto pulls = GetPulls(resultTree, muTruth, sigmaTruth, slopeTruth, nsigTruth, nbkgTruth);
    for (const auto& keyPullPair : pulls){
        auto fitOutput = FitPulls(keyPullPair.first, keyPullPair.second);
        auto mean = std::get<0>(fitOutput);
        auto sigma = std::get<1>(fitOutput);
        auto plot = std::get<2>(fitOutput);
        means[keyPullPair.first] = mean;
        sigmas[keyPullPair.first] = sigma;
        std::string plotName = keyPullPair.first + "." + extension;
        DrawPlot(mean, sigma, plot, plotName);
        delete plot;
    }

    DumpPulls(means, sigmas, outputfile);
    for (auto& pair : means){
        const auto& key = pair.first;
        delete means[key];
        delete sigmas[key];
    }

    return 0;
}

#endif
