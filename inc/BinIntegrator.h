#ifndef BININTEGRATOR_H
#define BININTEGRATOR_H

namespace BinIntegrator{
    double evaluatePDF(double x, void * params);
}

#endif
