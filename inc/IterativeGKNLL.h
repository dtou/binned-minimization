#ifndef ITERATIVEGKNLL_H
#define ITERATIVEGKNLL_H

#include "RooRealVar.h"
#include "RooNLLVar.h"
#include "RooAbsPdf.h"
#include "RooDataHist.h"
#include "RooCmdArg.h"

#include <vector>
#include <functional>
#include <cmath>
#include <gsl/gsl_integration.h>

namespace BinnedIterativeGKNLL{
    RooAbsReal * CreateNLL(RooAbsPdf& _model, RooDataHist& _binnedData, RooRealVar& observable, const RooLinkedList& cmdList);
    RooAbsReal * CreateNLL(RooAbsPdf& _model, RooDataHist& _binnedData, RooRealVar& observable, 
                const RooCmdArg& arg1=RooCmdArg::none(),  const RooCmdArg& arg2=RooCmdArg::none(),  
                const RooCmdArg& arg3=RooCmdArg::none(),  const RooCmdArg& arg4=RooCmdArg::none(), const RooCmdArg& arg5=RooCmdArg::none(),  
                const RooCmdArg& arg6=RooCmdArg::none(),  const RooCmdArg& arg7=RooCmdArg::none(), const RooCmdArg& arg8=RooCmdArg::none());
    RooArgSet* getAllConstraints(RooAbsPdf& model, const RooArgSet& observables, RooArgSet& constrainedParams, Bool_t stripDisconnected);
}

class IterativeGKNLL : public RooNLLVar {

public:
    // Constructors, assignment etc
    IterativeGKNLL();
    IterativeGKNLL(const char *name, const char* title, RooAbsPdf& pdf, RooDataHist& data, RooRealVar& observable,
                  const RooCmdArg& arg1=RooCmdArg::none(), const RooCmdArg& arg2=RooCmdArg::none(),const RooCmdArg& arg3=RooCmdArg::none(),
                  const RooCmdArg& arg4=RooCmdArg::none(), const RooCmdArg& arg5=RooCmdArg::none(),const RooCmdArg& arg6=RooCmdArg::none(),
                  const RooCmdArg& arg7=RooCmdArg::none(), const RooCmdArg& arg8=RooCmdArg::none(),const RooCmdArg& arg9=RooCmdArg::none());

    IterativeGKNLL(const char *name, const char *title, RooAbsPdf& pdf, RooDataHist& data, RooRealVar& observable,
                 Bool_t extended, const char* rangeName=0, const char* addCoefRangeName=0, 
                 Int_t nCPU=1, RooFit::MPSplit interleave=RooFit::BulkPartition, Bool_t verbose=kTRUE, 
                 Bool_t splitRange=kFALSE, Bool_t cloneData=kTRUE, Bool_t binnedL=kFALSE);
  
    IterativeGKNLL(const char *name, const char *title, RooAbsPdf& pdf, RooDataHist& data, RooRealVar& observable,
                  const RooArgSet& projDeps, Bool_t extended=kFALSE, const char* rangeName=0, 
                  const char* addCoefRangeName=0, Int_t nCPU=1, RooFit::MPSplit interleave=RooFit::BulkPartition, Bool_t verbose=kTRUE, 
                  Bool_t splitRange=kFALSE, Bool_t cloneData=kTRUE, Bool_t binnedL=kFALSE);

    IterativeGKNLL(const IterativeGKNLL& other, const char* name=0);


protected:
    Double_t evaluatePartition(std::size_t firstEvent, std::size_t lastEvent, std::size_t stepSize) const;

private:
    void SetObservable(RooRealVar& observable);
    void StoreBinning();
    void InitGSLFunction();
    double EvaluatePDF(double x, void * params);

private:
    // std::execution::parallel_unsequenced_policy m_executionPolicy;
    std::vector <double> binBoundaries;
    mutable std::vector <double> m_mu;
    mutable std::vector <double> m_n;
    mutable RooRealVar * m_observable;
    mutable bool m_first;
    bool m_extended;
    
    mutable std::vector <void *> m_gslParams;
    gsl_function m_gslFunction;

    const std::function<double(double, double)> masked_likelihood = [] (double mu, double n) { return (mu > 1e-15 && n > 1e-10) ? -n * log(mu) : 0; };
};

#endif
