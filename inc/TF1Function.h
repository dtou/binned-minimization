#ifndef TF1FUNCTION_H
#define TF1FUNCTION_H

class TF1Function{
public:
    TF1Function();
    TF1Function(int nBins, double xmin, double xmax);
    void SetNBins(int nbins);
    void SetMin(double min);
    void SetMax(double max);
    double operator() (double *x, double *p) const ;
    // The index and parameter they correspond to
    // 0 - Signal Yield
    // 1 - Background Yield
    // 2 - Signal 'mu'
    // 3 - Signal 'sigma'
    // 4 - Background 'slope'
    // 5 - Signal 'alpha low'
    // 6 - Signal 'n low'
    // 7 - Signal 'alpha high'
    // 8 - Signal 'n high'

private:
    double GetNormalisationExponential(double slope) const;
    double Compute_DSCB_PDF(double x, double mean, double sigma,
                            double alphaLow, double nLow,
                            double alphaHigh, double nHigh) const;
    double GetNormalisationDSCB(double mean, double sigma,
                            double alphaLow, double nLow,
                            double alphaHigh, double nHigh) const;
    double Compute_DSCB_CDF(double x, double mean, double sigma,
                            double alphaLow, double nLow,
                            double alphaHigh, double nHigh) const;

private:
    const double invSqrt2 = 0.70710678;
    const double sqrtPi   = 1.7724539;
    int m_nbins;
    double m_xmin;
    double m_xmax;
};

#endif
