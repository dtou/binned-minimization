#ifndef CUBICSPLINE_H
#define CUBICSPLINE_H

#include <vector>

class CubicSpline{
public:
    CubicSpline() = default;
     std::vector <double> operator()(const std::vector <double>& x, const std::vector <double>& y) const;
};

#endif
