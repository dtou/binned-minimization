#ifndef TF1MODEL_H
#define TF1MODEL_H

#include "TF1Function.h"
#include "RooRealVar.h"
#include "TF1.h"

class TF1Model{
public:

    TF1Model();
    TF1Model(const RooRealVar& observable);
    void SetRange(const RooRealVar& observable);
    void ConfigureSignal(int n_signal, double mean, double sigma,
                         double aLow, double nLow, double aHigh, double nHigh);
    void ConfigureBackground(int n_background, double slope);
    void ResetVariables();
    TF1 * GetTF1Model();
    ~TF1Model();

private:
    void DeleteTF1IfNotNull();

private:
    TF1 * m_model;
    TF1Function m_function;

    double m_xmin;
    double m_xmax;

    double m_initialNSig;
    double m_initialNBkg;
    double m_initialMu;
    double m_initialSigma;
    double m_initialSlope;
    double m_aLow;
    double m_nLow;
    double m_aHigh;
    double m_nHigh;
};

#endif
