#ifndef TF1RESULTLOGGER_H
#define TF1RESULTLOGGER_H

#include "TFile.h"
#include "TTree.h"
#include "TMatrixDSym.h"
#include <vector>
#include "TFitResultPtr.h"
#include "TString.h"

/**
 * \brief A struct which helps factorise data handling when dumping results into an nTuple.
 */
struct singleFitResult_t {
    double *   values;
    double *   errors;
    int        covarianceQuality;
    int        fitStatus;
    double     edm;
    double     minNll;
    int        numInvalidNLL;
   TMatrixDSym correlationMatrix;
   TMatrixDSym covarianceMatrix;
};

class TF1ResultLogger{
    public:
        TF1ResultLogger() = default;
        void LogFit(const TFitResultPtr & fitResult);
        void SaveResults(TString fileName, TString treeName); 

    private:
        void CheckResultConsistency(const TFitResultPtr & fitResult);
        void RecordParNames(const TFitResultPtr & fitResult);
        void RecordNPars(const TFitResultPtr & fitResult);
        void CheckParCount(const TFitResultPtr & fitResult) const;
        void CheckParNames(const TFitResultPtr & fitResult) const;
        void LogVariables(const TFitResultPtr & fitResult);
        void LogFitStatus(const TFitResultPtr & fitResult);
        void LogMatrices(const TFitResultPtr & fitResult);
        void CheckLogStarted() const ;
        void InstantiateOutputs(const TString & fileName, const TString & treeName);
        void AllocateContainers();
        void AttachBranchesToTree();
        void AddBranch(const TString & name, int index);
        TString RemoveMathFormula(TString initialName) const;
        void FillTree();
        int CalculateNumberOfFits() const;
        void FillEvent(int index);
        void FillParNameTree();
        void WriteAndClose();
        void PrintSuccessfulOutput(const TString & fileName, const TString & treeName) const ;

    private:
        static const TString parListBranchName;
        static const TString parListTreeName;

        std::vector< TString >         m_listOfParNames;
        std::vector< double >          m_values;
        std::vector< double >          m_errors;
        std::vector< int >             m_covarianceQualities;
        std::vector< int >             m_fitStatuses;
        std::vector< double >          m_edm;
        std::vector< double >          m_minNll;
        std::vector< TMatrixDSym >     m_correlationMatrices;
        std::vector< TMatrixDSym >     m_covarianceMatrices;

        TFile *           m_outFile     = nullptr;
        TTree *           m_outTree     = nullptr;
        TTree *           m_parNameTree = nullptr;
        singleFitResult_t m_singleFitResult;
        int               m_numberOfVariables;
};

#endif
