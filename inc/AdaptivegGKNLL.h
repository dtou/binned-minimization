#ifndef ADAPTIVEGGKNLL_H
#define ADAPTIVEGGKNLL_H

#include "RooRealVar.h"
#include "RooNLLVar.h"
#include "RooAbsPdf.h"
#include "RooDataHist.h"
#include "RooCmdArg.h"

#include <functional>
#include <cmath>
#include <gsl/gsl_integration.h>

namespace BinnedAdaptivegGKNLL{
    RooAbsReal * CreateNLL(RooAbsPdf& _model, RooDataHist& _binnedData, RooRealVar& observable, const RooLinkedList& cmdList);
    RooAbsReal * CreateNLL(RooAbsPdf& _model, RooDataHist& _binnedData, RooRealVar& observable, 
                const RooCmdArg& arg1=RooCmdArg::none(),  const RooCmdArg& arg2=RooCmdArg::none(),  
                const RooCmdArg& arg3=RooCmdArg::none(),  const RooCmdArg& arg4=RooCmdArg::none(), const RooCmdArg& arg5=RooCmdArg::none(),  
                const RooCmdArg& arg6=RooCmdArg::none(),  const RooCmdArg& arg7=RooCmdArg::none(), const RooCmdArg& arg8=RooCmdArg::none());
    RooArgSet* getAllConstraints(RooAbsPdf& model, const RooArgSet& observables, RooArgSet& constrainedParams, Bool_t stripDisconnected);
}


class AdaptivegGKNLL : public RooNLLVar {

public:
    // Constructors, assignment etc
    AdaptivegGKNLL();
    AdaptivegGKNLL(const char *name, const char* title, RooAbsPdf& pdf, RooDataHist& data, RooRealVar& observable,
                  const RooCmdArg& arg1=RooCmdArg::none(), const RooCmdArg& arg2=RooCmdArg::none(),const RooCmdArg& arg3=RooCmdArg::none(),
                  const RooCmdArg& arg4=RooCmdArg::none(), const RooCmdArg& arg5=RooCmdArg::none(),const RooCmdArg& arg6=RooCmdArg::none(),
                  const RooCmdArg& arg7=RooCmdArg::none(), const RooCmdArg& arg8=RooCmdArg::none(),const RooCmdArg& arg9=RooCmdArg::none());

    AdaptivegGKNLL(const char *name, const char *title, RooAbsPdf& pdf, RooDataHist& data, RooRealVar& observable,
                 Bool_t extended, const char* rangeName=0, const char* addCoefRangeName=0, 
                 Int_t nCPU=1, RooFit::MPSplit interleave=RooFit::BulkPartition, Bool_t verbose=kTRUE, 
                 Bool_t splitRange=kFALSE, Bool_t cloneData=kTRUE, Bool_t binnedL=kFALSE);
  
    AdaptivegGKNLL(const char *name, const char *title, RooAbsPdf& pdf, RooDataHist& data, RooRealVar& observable,
                  const RooArgSet& projDeps, Bool_t extended=kFALSE, const char* rangeName=0, 
                  const char* addCoefRangeName=0, Int_t nCPU=1, RooFit::MPSplit interleave=RooFit::BulkPartition, Bool_t verbose=kTRUE, 
                  Bool_t splitRange=kFALSE, Bool_t cloneData=kTRUE, Bool_t binnedL=kFALSE);

    AdaptivegGKNLL(const AdaptivegGKNLL& other, const char* name=0);

    ~AdaptivegGKNLL();

protected:
    Double_t evaluatePartition(std::size_t firstEvent, std::size_t lastEvent, std::size_t stepSize) const;

private:
    void SetObservable(RooRealVar& observable);
    void StoreBinning();
    void InitGSLWorkspace();
    void InitGSLFunction();
    double EvaluatePDF(double x, void * params);

private:
    std::vector <double> binBoundaries;
    mutable std::vector <double> m_mu;
    mutable std::vector <double> m_n;
    mutable RooRealVar * m_observable;
    mutable bool m_first;
    bool m_extended;
    
    mutable std::vector <void *> m_gslParams;
    gsl_function m_gslFunction;
    mutable gsl_integration_workspace * m_integrationWorkspace;
    // mutable double m_gslNormalisation;

    const std::function<double(double, double)> masked_likelihood = [] (double mu, double n) { return (mu > 1e-15 && n > 1e-10) ? -n * log(mu) : 0; };
};

#endif
