#ifdef __ROOTCLING__

#pragma link off all class;
//#pragma link off all enum;
//#pragma link off all function;
//#pragma link off all global;
#pragma link off all namespace;
//#pragma link off all struct;
//#pragma link off all typedef;
//#pragma link off all union;

#pragma link C++ nestedclasses;
#pragma link C++ nestedtypedef;

#pragma link C++ class RooDoubleSidedCBShape+;
#pragma link C++ defined_in "RooDoubleSidedCBShape.h";

#endif
